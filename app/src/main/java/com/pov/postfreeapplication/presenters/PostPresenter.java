package com.pov.postfreeapplication.presenters;

import com.pov.postfreeapplication.api.APIClient;
import com.pov.postfreeapplication.api.APIInterface;
import com.pov.postfreeapplication.models.dummy.post.PostResponse;
import com.pov.postfreeapplication.views.PostView;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PostPresenter {
    private APIInterface apiInterface;
    private PostView view;

    public PostPresenter(PostView view) {
        this.view = view;
        apiInterface = APIClient.getClient().create(APIInterface.class);
    }
    public void getAllPosts(){
        view.onLoading();
        apiInterface.getAllPosts().enqueue(new Callback<PostResponse>() {
            @Override
            public void onResponse(Call<PostResponse> call, Response<PostResponse> response) {
                view.onSuccess("Get data Success");
                view.onHiding();
                if (response.isSuccessful() && null != response.body()){
                    view.onGetDataSuccess(response.body().getPosts());
                }

            }

            @Override
            public void onFailure(Call<PostResponse> call, Throwable throwable) {
                view.onHiding();
                view.onError(throwable.getLocalizedMessage().toString());
            }
        });
    }
}
