package com.pov.postfreeapplication.api;

import com.pov.postfreeapplication.models.dummy.post.PostResponse;

import retrofit2.Call;
import retrofit2.http.GET;

public interface APIInterface {
    @GET("/posts")
    Call<PostResponse> getAllPosts();
}
