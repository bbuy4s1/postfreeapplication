package com.pov.postfreeapplication.views;

import com.pov.postfreeapplication.models.dummy.post.PostsItem;

import java.util.List;

public interface PostView {
    void onLoading();
    void onHiding();
    void onError(String message);
    void onSuccess(String message);
    void onGetDataSuccess(List<PostsItem> data);
}
