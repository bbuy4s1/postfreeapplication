package com.pov.postfreeapplication.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.pov.postfreeapplication.R;
import com.pov.postfreeapplication.models.dummy.post.PostsItem;

import java.util.List;

public class PostAdapter extends RecyclerView.Adapter<PostAdapter.PostViewHolder>{
    private Context context;
    private List<PostsItem> postsItemList;

    public PostAdapter(Context context, List<PostsItem> postsItemList) {
        this.context = context;
        this.postsItemList = postsItemList;
    }

    @NonNull
    @Override
    public PostViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.post_card_item_layout,null,false);
        return new PostViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull PostViewHolder holder, int position) {
        PostsItem postsItem = postsItemList.get(position);
        if (!postsItem.getTitle().isEmpty()){
            holder.title.setText(postsItem.getTitle());
        }
    }

    @Override
    public int getItemCount() {
        return postsItemList.size();
    }

    public static class PostViewHolder extends RecyclerView.ViewHolder{
        TextView title;
        public PostViewHolder(@NonNull View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.postTitle);
        }
    }
}
